import nltk
import csv
import matplotlib.pyplot as plt
import PySimpleGUI as sg
import gensim
import gensim.corpora as corpora
import numpy as np
import sklearn as sk
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg
import tkinter as Tk
from collections import Counter

def getWords(data, data2):
    words = []
    tweets1 = []
    tweets2 = []
    tweetsnhs1 = []
    tweetsnhs2 = []
    for i in range(0, len(data)):
        newdata = (list(filter(None, data[i])))
        if ("brexit" in newdata):
            tweets1.append(newdata)
        if ("nhs" in newdata):
            tweetsnhs1.append(newdata)
        for j in range(0, len(data[i])):
            if (data[i][j] != ""):
                words.append(data[i][j])
    words2 = []
    for i in range(0, len(data2)):
        newdata = (list(filter(None, data2[i])))
        if ("brexit" in newdata):
            tweets2.append(newdata)
        if ("nhs" in newdata):
            tweetsnhs2.append(newdata)
        for j in range(0, len(data2[i])):
            if (data2[i][j] != ""):
                words2.append(data2[i][j])
    words3 = words + words2
    return words, words2, words3, tweets1,tweets2,tweetsnhs1,tweetsnhs2

def makemodel(wordlist):
    id2word = corpora.Dictionary([wordlist])

    # Create Corpus
    texts = [wordlist]

    # Term Document Frequency
    corpus = [id2word.doc2bow(text) for text in texts]
    lda_model = gensim.models.ldamodel.LdaModel(corpus=corpus,
                                               id2word=id2word,
                                               num_topics=5)
    return lda_model
def getwords(model):
    x=model.show_topics(formatted=False)
    topics_words = (sum([([wd[0] for wd in tp[1]]) for tp in x], []))
    return topics_words

def listSenti(sentilist):
    tuples = []
    for text in sentilist:
        positive = text[-25]
        negative = ("-"+text[-22])
        tuples.append([int(positive),int(negative)])
    return tuples

def plotSenti(list1, title):
    fig, axs = plt.subplots(3, 1, constrained_layout=True,figsize=(6,8))
    plt.figure(figsize=(9,7))
    x,y = zip(*list1)
    new_x, new_y = zip(*sorted(zip(x, y)))
    axs[0].set_title('The sentiment value pairs of tweets')
    axs[0].scatter(new_x, new_y)
    axs[0].invert_yaxis()
    axs[0].set_xlabel("Positive sentiment strength")
    axs[0].set_ylabel("Negative sentiment strength")
    axs[1].set_title('Histogram of the positive values')
    axs[1].hist(new_x)
    axs[2].set_title('Histogram of the negative values')
    axs[2].hist(new_y)
    axs[2].invert_xaxis()
    fig.suptitle(title, fontsize=10)
    #fig.savefig('sentilabour.png')

    #fig = plt.gcf()
    figure_x, figure_y, figure_w, figure_h = fig.bbox.bounds
    return fig, figure_x, figure_y, figure_w, figure_h

def plotTuples(list1, list2, list3, title):
    fig, axs = plt.subplots(3, 1, constrained_layout=True,figsize=(8,8))
    plt.figure(figsize=(6,8))
    axs[0].set_title('Verbs')
    axs[0].bar(*zip(*list1))
    axs[1].set_title('Adverbs')
    axs[1].bar(*zip(*list2))
    axs[2].set_title('Adjectives')
    axs[2].bar(*zip(*list3))
    fig.suptitle(title, fontsize=10)
    #fig.savefig('labourwordnhs.png')

    figure_x, figure_y, figure_w, figure_h = fig.bbox.bounds
    return fig, figure_x, figure_y, figure_w, figure_h

def countNegation(wordlist):
    operators = ["no", "not", "none", "never", "nobody", "nothing", "neither", "nobody", "no", "none", "nor", "nowhere","don't","can't","won't","doesn't", "aren't", "isn't",
                "didn't","haven't","hadn't","shouldn't","wouldn't","won't"]
    found = []
    for i in range(len(wordlist)):
        for operator in operators:
            if(operator in wordlist[i]):
                found.append(operator)
    return Counter(found).most_common()
def countNegationNextTo(wordlist, word):
    operators = ["no", "not", "none", "never", "nobody", "nothing", "neither", "nobody", "no", "none", "nor", "nowhere","don't","can't","won't","doesn't", "aren't", "isn't",
                "didn't","haven't","hadn't","shouldn't","wouldn't","won't"]
    found = []
    for i in range(len(wordlist)):
        for operator in operators:
            if((operator in wordlist[i])):
                if((operator==wordlist[wordlist[i].index(word)-1])or(operator==wordlist[wordlist[i].index(word)+1])):
                    found.append(operator)
    return Counter(found).most_common()
def pearson(list1,list2):
        #,y = zip(*list1)
        #2,y2 = zip(*list2)
        #1 = (sum(x)/len(x),sum(y)/len(y))
        #2 = (sum(x2)/len(x2),sum(y2)/len(y2))
        l1 = []
        l2 = []
        for a in list1:
            l1.append(sum(a))
        for b in list2:
            l2.append(sum(b))
        #rint(l1)
        #rint(l2)
        print("Pearson correlation between the datasets")
        #j = list2[:len(list1)]
        return(np.corrcoef(sorted(l1), sorted(l2[:len(l1)]))[0, 1])
        #print(scipy.stats.pearsonr(np.array(list1),np.array(list1)))
def getBestToken(tokens,num):
    best=(0,100)
    for a in tokens:
        if(abs(num-a[1])<abs(num-best[1])):
            best=a
    return best[0]

def getPartOfSpeech(sentences,indexword):
    verbs = []
    adverbs = []
    adjectives = []
    for i in range(len(sentences)):
        tagged = nltk.pos_tag(sentences[i])
        vb = []
        rb = []
        jj = []
        for j in range(len(tagged)):
            word = tagged[j][0]
            tag = tagged[j][1]
            if(word==indexword):
                num=j
            elif(tag.startswith("VB")):
                vb.append((word,j))
            elif(tag.startswith("RB")):
                rb.append((word,j))
            elif(tag.startswith("JJ")):
                jj.append((word,j))
        bestvb=getBestToken(vb,num)
        if(bestvb!=0):
            verbs.append(bestvb)
        bestrb=getBestToken(rb,num)
        if(bestrb!=0):
            adverbs.append(bestrb)
        bestjj=getBestToken(jj,num)
        if(bestjj!=0):
            adjectives.append(bestjj)
    return verbs, adverbs, adjectives


def draw_figure(canvas, figure, loc=(0, 0)):
    figure_canvas_agg = FigureCanvasTkAgg(figure, canvas)
    figure_canvas_agg.draw()
    figure_canvas_agg.get_tk_widget().pack(side='top', fill='both', expand=1)
    return figure_canvas_agg


class GUI():

    def __init__(self):
        with open('conservativestem.csv') as csvfile:
            self.dataset1 = list(csv.reader(csvfile))

        with open('labourstem.csv') as csvfile:
            self.dataset2 = list(csv.reader(csvfile))
        self.words1, self.words2, self.words3, self.tweets1, self.tweets2, self.tweetsnhs1, self.tweetsnhs2 = getWords(self.dataset1,self.dataset2)
        self.button = 0
        self.endloop = False
        self.window = None
        file1 = open("conservativestemtxt+results.txt", "r")
        self.conssenti = file1.readlines()  # -25 -22
        file2 = open("labourstemtxt+results.txt", "r")
        self.laboursenti = file2.readlines()

    def openingView(self):

        layout = [
            [sg.Button("Top words"), sg.Button("Topics"), sg.Button("SentiStrength"), sg.Button("POS statistics"), sg.Button("Negation statistics"), sg.Button("Exit")],
        ]
        window1 = sg.Window(" ").Layout(layout)
        if (self.window is not None):
            self.window.Close()
        self.window = window1
        button, values = self.window.Read()
        print(button)
        if button is None or button == 'Exit':
            self.endloop = True
        elif (button == "Top words"):
            self.commonWords()
        elif (button == "Topics"):
            self.topicScreen()
        elif (button == "SentiStrength"):
            self.sentiScreen()
        elif (button == "POS statistics"):
            self.posScreen()
        elif (button == "Negation statistics"):
            self.negScreen()
    def commonWords(self):
        print("vitthyy")
        fdist = nltk.FreqDist((self.words1))
        fdist2 = nltk.FreqDist((self.words2))
        #fdist3 = nltk.FreqDist((self.words3))
        conspartytop = fdist.most_common()
        labpartytop = fdist2.most_common()
        conslist = []
        labrlist = []
        for a in conspartytop:
            conslist.append(str(a))
        for b in labpartytop:
            labrlist.append(str(b))

        consset = set()
        labset = set()
        for i in range(0, 10):
            consset.add(conspartytop[i][0])
            labset.add(labpartytop[i][0])
        jacindex = 1 - nltk.jaccard_distance(consset, labset)

        layout = [[sg.Text('The words and their occurances according to party')],
        [sg.Text('The Conservative party                                                                The Labour party')],
                  [sg.Listbox(values=conslist,size=(40, 24), key='-LIST-', enable_events=False),
                  sg.Listbox(values=labrlist, size=(40, 24), key='-LIST-', enable_events=False)],
                  [sg.Text('The jacquard index between the 10 most common words in the lists: '+str(jacindex))],
                  [sg.Button('Back')]]

        window1 = sg.Window(" ").Layout(layout)
        self.window.Close()
        self.window = window1
        button, values = self.window.Read()
        if(button == "Back"):
                self.openingView()


    def topicScreen(self):

        lda_model1 = makemodel(self.words1)
        lda_model2 = makemodel(self.words2)
        constopics = lda_model1.show_topics(formatted=False)
        labrtopics = lda_model2.show_topics(formatted=False)
        consmodel = getwords(lda_model1)
        labrmodel = getwords(lda_model2)
        jacindex = 1 - nltk.jaccard_distance(set(consmodel), set(labrmodel))

        layout = [[sg.Text('The topics of the political parties with the words and weights of the words')],
                  [sg.Text('The Conservative party')],
                  [sg.Listbox(values=constopics, size=(200, 10), key='-LIST-', enable_events=False)],
                  [sg.Text('The Labour party')],

                  [sg.Listbox(values=labrtopics, size=(200, 10), key='-LIST-', enable_events=False)],
                  [sg.Text('The jacquard index between the words occurring in the topics: ' + str(jacindex))],
                  [sg.Button('Back')]]
        window1 = sg.Window(" ").Layout(layout)
        self.window.Close()
        self.window = window1
        button, values = self.window.Read()
        if (button == "Back"):
            self.openingView()

    def sentiScreen(self):

        consenti = listSenti(self.conssenti)
        labsenti = listSenti(self.laboursenti)
        pson = pearson(consenti,labsenti)
        fig, figure_x, figure_y, figure_w, figure_h = plotSenti(consenti,"The Conservative party statistics")

        fig2, figure_x2, figure_y2, figure_w2, figure_h2 = plotSenti(labsenti,"The Labour party statistics")

        layout = [[sg.Text('Tweets analyzed by SentiStrength')],
                  [sg.Canvas(key='canvas'),
                   sg.Canvas(key='canvas2')],
                  [sg.Text('The pearson correlation between the sentiStrength values:'+str(pson))],
                  [sg.Button('Back')]]

        # create the form and show it without the plot
        self.window = sg.Window('Demo Application - Embedding Matplotlib In PySimpleGUI', force_toplevel=False).Layout(
            layout).Finalize()

        fig_photo = draw_figure(self.window.FindElement('canvas').TKCanvas, fig)
        fig_photo = draw_figure(self.window.FindElement('canvas2').TKCanvas, fig2)

        button, values = self.window.Read()
        if (button == "Back"):
            self.openingView()


    def posScreen(self):

        layout = [
            [sg.Text("Pick the keyword")],
            [sg.Button("Brexit"), sg.Button("NHS"), sg.Button("Back"),
             sg.Button("Exit")],
        ]
        window1 = sg.Window(" ").Layout(layout)
        self.window.Close()
        self.window = window1
        button, values = self.window.Read()
        if (button == "Back"):
            self.openingView()
        elif(button == "Brexit"):
            self.brextScreen()
        elif (button == "NHS"):
            self.nhsScreen()
    def brextScreen(self):

        verbs1, adverbs1, adjectives1 = getPartOfSpeech(self.tweets1, "brexit")
        verbs2, adverbs2, adjectives2 = getPartOfSpeech(self.tweets2, "brexit")

        freqverb1 = nltk.FreqDist((verbs1))
        freqadv1 = nltk.FreqDist((adverbs1))
        freqadj1 = nltk.FreqDist((adjectives1))

        freqverb2 = nltk.FreqDist((verbs2))
        freqadv2 = nltk.FreqDist((adverbs2))
        freqadj2 = nltk.FreqDist((adjectives2))

        title = "Most co-occuring terms with the word ""Brexit"" in the Conservative party."""
        fig, figure_x, figure_y, figure_w, figure_h = plotTuples(freqverb1.most_common(10),freqadv1.most_common(10),freqadj1.most_common(10),title)

        title = "Most co-occuring terms with the word ""Brexit"" in the Labour party."""
        fig2, figure_x2, figure_y2, figure_w2, figure_h2 = plotTuples(freqverb2.most_common(10),freqadv2.most_common(10),freqadj2.most_common(10),title)


        layout = [[sg.Text('Co-occuring words with the word Brexit')],
                  [sg.Canvas(key='canvas'),
                   sg.Canvas(key='canvas2')],
                  [sg.Button('Back')]]

        # create the form and show it without the plot
        self.window = sg.Window('Demo Application - Embedding Matplotlib In PySimpleGUI', force_toplevel=False).Layout(
            layout).Finalize()

        fig_photo = draw_figure(self.window.FindElement('canvas').TKCanvas, fig)
        fig_photo = draw_figure(self.window.FindElement('canvas2').TKCanvas, fig2)

        button, values = self.window.Read()
        if (button == "Back"):
            self.posScreen()

    def nhsScreen(self):

        verbs1, adverbs1, adjectives1 = getPartOfSpeech(self.tweetsnhs1, "nhs")
        verbs2, adverbs2, adjectives2 = getPartOfSpeech(self.tweetsnhs2, "nhs")

        freqverb1 = nltk.FreqDist((verbs1))
        freqadv1 = nltk.FreqDist((adverbs1))
        freqadj1 = nltk.FreqDist((adjectives1))

        freqverb2 = nltk.FreqDist((verbs2))
        freqadv2 = nltk.FreqDist((adverbs2))
        freqadj2 = nltk.FreqDist((adjectives2))

        title = "Most co-occuring terms with the word ""NHS"" in the Conservative party."""
        fig, figure_x, figure_y, figure_w, figure_h = plotTuples(freqverb1.most_common(10),freqadv1.most_common(10),freqadj1.most_common(10),title)

        title = "Most co-occuring terms with the word ""NHS"" in the Labour party."""
        fig2, figure_x2, figure_y2, figure_w2, figure_h2 = plotTuples(freqverb2.most_common(10),freqadv2.most_common(10),freqadj2.most_common(10),title)


        layout = [[sg.Text('Co-occuring words with the word NHS')],
                  [sg.Canvas(key='canvas'),
                   sg.Canvas(key='canvas2')],
                  [sg.Button('Back')]]

        # create the form and show it without the plot
        self.window = sg.Window('Demo Application - Embedding Matplotlib In PySimpleGUI', force_toplevel=False).Layout(
            layout).Finalize()

        fig_photo = draw_figure(self.window.FindElement('canvas').TKCanvas, fig)
        fig_photo = draw_figure(self.window.FindElement('canvas2').TKCanvas, fig2)

        button, values = self.window.Read()
        if (button == "Back"):
            self.posScreen()

    def negScreen(self):

        consbrcount = countNegation(self.tweets1)
        labourbrcount = countNegation(self.tweets2)

        consbrcount2 = countNegationNextTo(self.tweets1,"brexit")
        labourbrcount2 = countNegationNextTo(self.tweets2,"brexit")

        consbrcountnhs = countNegation(self.tweetsnhs1)
        labourbrcountnhs = countNegation(self.tweetsnhs2)

        consbrcountnhs2 = countNegationNextTo(self.tweetsnhs1,"nhs")
        labourbrcountnhs2 = countNegationNextTo(self.tweetsnhs2,"nhs")

        tab1_layout = [[sg.T('The Conservatives             The Conservatives next to the word Brexit')],[sg.Listbox(values=consbrcount,size=(20, 12), key='-LIST-', enable_events=False),
                  sg.Listbox(values=consbrcount2, size=(20, 12), key='-LIST-', enable_events=False)],
                       [sg.Text('The Labour             The Labour next to the word Brexit')],[sg.Listbox(values=labourbrcount, size=(20, 12), key='-LIST-', enable_events=False),

                        sg.Listbox(values=labourbrcount2, size=(20, 12), key='-LIST-', enable_events=False)]
                       ]

        tab2_layout = [[sg.T('The Conservatives             The Conservatives next to the word NHS')],[sg.Listbox(values=consbrcountnhs,size=(20, 12), key='-LIST-', enable_events=False),
                  sg.Listbox(values=consbrcountnhs2, size=(20, 12), key='-LIST-', enable_events=False)],
                       [sg.Text('The Labour             The Labour next to the word NHS')],[sg.Listbox(values=labourbrcountnhs, size=(20, 12), key='-LIST-', enable_events=False),

                        sg.Listbox(values=labourbrcountnhs2, size=(20, 12), key='-LIST-', enable_events=False)]
                       ]

        layout = [[sg.Text("Most common negations in the same tweet as the keyword")],[sg.TabGroup([[sg.Tab('Brexit', tab1_layout, tooltip='tip'), sg.Tab('NHS', tab2_layout)]], tooltip='TIP2')],
                  [sg.Button('Back')]]
        window1 = sg.Window(" ").Layout(layout)
        self.window.Close()
        self.window = window1
        button, values = self.window.Read()
        if (button == "Back"):
            self.openingView()
if __name__ == "__main__":
    client = GUI()
    client.openingView()